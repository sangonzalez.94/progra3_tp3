package src.test.progra3_tp3.grafo.mapa;
import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import progra3_tp3.model.grafo.BFS;
import progra3_tp3.model.grafo.Grafo;
import progra3_tp3.model.grafo.Vertice;

public class BFSTests {
	
	@Test
	public void grafoEsConexo() {
		Grafo grafo = new Grafo();
		Vertice v1 = new Vertice("Buenos Aires");
		Vertice v2 = new Vertice("Buenos Aires Ciudad");
		grafo.agregarVertice(v1,v2);
		
		grafo.agregarArista(v1, v2);
		
		assertTrue(BFS.esConexo(grafo));
	}
	
	@Test
	public void verticesAlcanzadasDesdeUnGrafo() {
		// Primero seteo los vertices
		Vertice buenosAires, caba, cordoba, santaFe, laPampa, rioNegro, chubut, santaCruz;
		buenosAires = new Vertice("Buenos Aires");
		caba = new Vertice("CABA");
		cordoba = new Vertice("Cordoba");
		santaFe = new Vertice("Santa Fe");
		laPampa = new Vertice("La Pampa");
		rioNegro = new Vertice("Rio Negro");
		chubut = new Vertice("Chubut");
		santaCruz = new Vertice("Santa Cruz");
		// Segundo: armo el grafo
		Grafo grafo = new Grafo();

		// tercero agrego las vertices al grafo
		grafo.agregarVertice(buenosAires, caba, cordoba, santaFe, laPampa, rioNegro, chubut, santaCruz);

		// cuarto agrego las provincias limitrofes con sus respectivas similaridades
		grafo.agregarArista(buenosAires, caba);
		grafo.agregarArista(buenosAires, cordoba);
		grafo.agregarArista(buenosAires, santaFe);
		grafo.agregarArista(buenosAires, rioNegro);
		grafo.agregarArista(cordoba, santaFe);
		grafo.agregarArista(laPampa, buenosAires);
		grafo.agregarArista(laPampa, cordoba);
		grafo.agregarArista(rioNegro, laPampa);
		grafo.agregarArista(santaCruz, rioNegro);
		grafo.agregarArista(santaCruz, chubut);

		assertTrue(BFS.esConexo(grafo));
		Set<Vertice> ret =  BFS.alcanzablesDesdeUnVertice(grafo, buenosAires);
		assertTrue(ret.size()==8);
	}
}
