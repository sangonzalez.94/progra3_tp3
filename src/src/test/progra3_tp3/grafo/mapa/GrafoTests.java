package src.test.progra3_tp3.grafo.mapa;
import static org.junit.Assert.*;


import org.junit.Test;

import progra3_tp3.model.grafo.Grafo;
import progra3_tp3.model.grafo.Vertice;
public class GrafoTests {

	
	@Test
	public void crearGrafoYAnadirUnVertice() {
		Grafo grafo = new Grafo();
		Vertice v = new Vertice("Buenos Aires");
		grafo.agregarVertice(v);
		assertTrue("El grafo contiene un elemento", grafo.tamano()==1);
	}
	
	@Test
	public void crearGrafoYAnadirDosVerticesMismoNombreDistintoObjeto() {
		Grafo grafo = new Grafo();
		Vertice v = new Vertice("Buenos Aires");
		Vertice v2 = new Vertice("Buenos Aires");
		grafo.agregarVertice(v);
		grafo.agregarVertice(v2);
		assertTrue("El grafo contiene un elemento", grafo.tamano()==1);
	}
	
	@Test
	public void crearGrafoYAnadirDosVerticesDistintoNombre() {
		Grafo grafo = new Grafo();
		Vertice v = new Vertice("Buenos Aires");
		Vertice v2 = new Vertice("Santa Fe");
		grafo.agregarVertice(v);
		grafo.agregarVertice(v2);
		assertTrue("El grafo contiene dos elemento", grafo.tamano()==2);
		assertTrue(grafo.getVertices().size()==2);
	}
	
	
	@Test
	public void eliminarDosVerticesLimitrofesConUna() {
		//Primero seteo los vertices
		
		int[] arreglo = {2,3,4,5,6};
		Vertice buenosAires,caba,cordoba,santaFe,laPampa,rioNegro,chubut,santaCruz;
		buenosAires = new Vertice("Buenos Aires");
		caba = new Vertice("CABA");
		cordoba = new Vertice("Cordoba");
		santaFe = new Vertice("Santa Fe");
		laPampa = new Vertice("La Pampa");
		rioNegro = new Vertice("Rio Negro");
		chubut = new Vertice("Chubut");
		santaCruz = new Vertice("Santa Cruz");
		//Segundo: armo el grafo
		Grafo grafo = new Grafo();
		
		//tercero agrego las vertices al grafo
		grafo.agregarVertice(buenosAires, caba, cordoba, santaFe, laPampa, rioNegro, chubut, santaCruz);
		
		//cuarto agrego las provincias limitrofes con sus respectivas similaridades
		grafo.agregarArista(buenosAires, caba);
		grafo.agregarArista(buenosAires, cordoba);
		grafo.agregarArista(buenosAires, santaFe);
		grafo.agregarArista(buenosAires, rioNegro);
		grafo.agregarArista(cordoba, santaFe);
		grafo.agregarArista(laPampa, buenosAires);
		grafo.agregarArista(laPampa, cordoba );
		grafo.agregarArista(rioNegro, laPampa);
		grafo.agregarArista(santaCruz, rioNegro);
		grafo.agregarArista(santaCruz, chubut);
		grafo.eliminarVertice(cordoba);
		grafo.eliminarVertice(laPampa);
		//verifico los vecinos
		assertTrue(grafo.vecinosDeVertice(buenosAires).size()==3);
		
	}
	/**
	 * No son test unitarios. Simulan ser tests de uso real
	 */
	@Test
	public void casoPruebaSimilarReal() {
		//Primero seteo los vertices
		Vertice buenosAires,caba,cordoba,santaFe,laPampa,rioNegro,chubut,santaCruz;
		buenosAires = new Vertice("Buenos Aires");
		caba = new Vertice("CABA");
		cordoba = new Vertice("Cordoba");
		santaFe = new Vertice("Santa Fe");
		laPampa = new Vertice("La Pampa");
		rioNegro = new Vertice("Rio Negro");
		chubut = new Vertice("Chubut");
		santaCruz = new Vertice("Santa Cruz");
		//Segundo: armo el grafo
		Grafo grafo = new Grafo();
		
		//tercero agrego las vertices al grafo
		grafo.agregarVertice(buenosAires, caba, cordoba, santaFe, laPampa, rioNegro, chubut, santaCruz);
		
		//cuarto agrego las provincias limitrofes con sus respectivas similaridades
		grafo.agregarArista(buenosAires, caba);
		grafo.agregarArista(buenosAires, cordoba);
		grafo.agregarArista(buenosAires, santaFe);
		grafo.agregarArista(buenosAires, rioNegro);
		grafo.agregarArista(cordoba, santaFe);
		grafo.agregarArista(laPampa, buenosAires);
		grafo.agregarArista(laPampa, cordoba);
		grafo.agregarArista(rioNegro, laPampa);
		grafo.agregarArista(santaCruz, rioNegro);
		grafo.agregarArista(santaCruz, chubut);
		
		//verifico los vecinos
		assertTrue(grafo.vecinosDeVertice(buenosAires).size()==5);
		
	}
	

	@Test
	public void crearGrafoYAnadirDosVerticesMismoNombreConDistintasMayusculas() {
		Grafo grafo = new Grafo();
		Vertice v = new Vertice("Buenos Aires");
		Vertice v2 = new Vertice("Buenos aires");
		grafo.agregarVertice(v);
		grafo.agregarVertice(v2);
		assertTrue("El grafo contiene un elemento", grafo.tamano()==1);
	}
}
