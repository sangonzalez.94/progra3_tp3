package src.test.progra3_tp3.grafo.mapa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import progra3_tp3.model.grafo.ConjuntoDominanteMinimo;
import progra3_tp3.model.grafo.Grafo;
import progra3_tp3.model.grafo.Vertice;

public class ConjuntoDominanteMinimoTests {

	private Grafo grafo;

	private ConjuntoDominanteMinimo conjuntoDominanteMinimo;
	
	@Before
	public void setUp() {
		conjuntoDominanteMinimo = new ConjuntoDominanteMinimo();
	}

	/* Casos de prueba */
	@Test
	public void conjuntoDominanteMinimoEjemploTP() {
		setUpDatosConjuntoDominanteMinimoEjemploTP();
		validacionCasoPruebaConjuntoDominanteMinimoEjemploTP();
	}

	@Test
	public void conjuntoDominanteMinimoConjuntoResultadoUnUnicoElementoGrafoConCuatro() {
		setUpDatosConjuntoDominanteMinimoConjuntoResultadoUnUnicoElementoGrafoConCuatro();
		validacionCasoPruebaConjuntoDominanteMinimoConjuntoResultadoUnUnicoElementoGrafoConCuatro();
	}
	
	@Test (expected = Exception.class)
	public void conjuntoDominanteMinimoConjuntoExcepcionPorNoConexo() {
		setUpDatosConjuntoDominanteMinimoConjuntoExcepcionPorNoConexo();
	}


	@Test
	public void conjuntoDominanteMinimoConjuntoResultadoVariosElementosCon14Vertices() {
		setUpDatosConjuntoDominanteMinimoConjuntoResultadoVariosElementosCon15VerticesYConSol6();
		validacionCasoPruebaConjuntoDominanteMinimoConjuntoResultadoVariosElementosCon15VerticesYConSol6();
	}
	/* Configuracion de las variables para cada caso de prueba */
	
	/*CASO DE PRUEBAS 1*/
	private void setUpDatosConjuntoDominanteMinimoEjemploTP() {
		grafo = new Grafo();
		Vertice v1 = new Vertice("1");
		Vertice v2 = new Vertice("2");
		Vertice v3 = new Vertice("3");
		Vertice v4 = new Vertice("4");
		Vertice v5 = new Vertice("5");
		Vertice v6 = new Vertice("6");

		grafo.agregarVertice(v1, v2, v3, v4, v5, v6);

		grafo.agregarArista(v1, v2);
		grafo.agregarArista(v1, v5);
		grafo.agregarArista(v2, v3);
		grafo.agregarArista(v2, v5);
		grafo.agregarArista(v3, v4);
		grafo.agregarArista(v4, v6);
		grafo.agregarArista(v4, v5);

	}

	private void validacionCasoPruebaConjuntoDominanteMinimoEjemploTP() {
		List<Vertice> conjuntoResultadoEsperado = new ArrayList<>();
		conjuntoResultadoEsperado.add(new Vertice("2"));
		conjuntoResultadoEsperado.add(new Vertice("4"));
		List<Vertice> conjuntoResultadoObtenido = conjuntoDominanteMinimo.obtenerConjuntoDominanteMinimo(grafo);
		
		assertEquals("El conjunto esperado debe ser 2 y 4", conjuntoResultadoEsperado, conjuntoResultadoObtenido);
	//	assertTrue(conjuntoResultadoObtenido.containsAll(conjuntoResultadoEsperado));
		
	}
	/*Fin caso de pruebas 1*/
	
	/*CASO DE PRUEBAS 2*/
	private void setUpDatosConjuntoDominanteMinimoConjuntoResultadoUnUnicoElementoGrafoConCuatro() {
		grafo = new Grafo();
		Vertice v1 = new Vertice("1");
		Vertice v2 = new Vertice("2");
		Vertice v3 = new Vertice("3");
		Vertice v4 = new Vertice("4");
		grafo.agregarVertice(v1, v2, v3, v4);

		grafo.agregarArista(v1, v2);
		grafo.agregarArista(v2, v4);
		grafo.agregarArista(v2, v3);
		grafo.agregarArista(v3, v4);
	}

	private void validacionCasoPruebaConjuntoDominanteMinimoConjuntoResultadoUnUnicoElementoGrafoConCuatro() {
		List<Vertice> conjuntoResultadoEsperado = new ArrayList<>();
		conjuntoResultadoEsperado.add(new Vertice("2"));
		List<Vertice> conjuntoResultadoObtenido = conjuntoDominanteMinimo.obtenerConjuntoDominanteMinimo(grafo);
		
		assertEquals("El conjunto esperado debe ser 2", conjuntoResultadoEsperado, conjuntoResultadoObtenido);		
	} 
	/*Fin caso de pruebas 2	*/
	
	/*CASO DE PRUEBAS 3*/
	private void setUpDatosConjuntoDominanteMinimoConjuntoExcepcionPorNoConexo() {
		grafo = new Grafo();
		Vertice v1 = new Vertice("1");
		Vertice v2 = new Vertice("2");
		Vertice v3 = new Vertice("3");
		Vertice v4 = new Vertice("4");
		Vertice v5 = new Vertice("5");
		Vertice v6 = new Vertice("6");
		Vertice v7 = new Vertice("7");
		Vertice v8 = new Vertice("8");
		Vertice v9 = new Vertice("9");
		Vertice v10 = new Vertice("10");

		grafo.agregarVertice(v1, v2, v3, v4, v5, v6, v7, v8, v9, v10);

		grafo.agregarArista(v1, v9);
		grafo.agregarArista(v8, v7);
		grafo.agregarArista(v7, v6);
		grafo.agregarArista(v3, v7);
		grafo.agregarArista(v3, v5);
		grafo.agregarArista(v3, v2);
		grafo.agregarArista(v4, v5);
		conjuntoDominanteMinimo.obtenerConjuntoDominanteMinimo(grafo);
	}

	
	/*Fin caso de pruebas 3	*/

	
	/*Fin caso de pruebas 4*/
	
	
	/*CASO DE PRUEBAS 5*/
	private void setUpDatosConjuntoDominanteMinimoConjuntoResultadoVariosElementosCon15VerticesYConSol6() {
		grafo = new Grafo();
		Vertice v1 = new Vertice("1");
		Vertice v2 = new Vertice("2");
		Vertice v3 = new Vertice("3");
		Vertice v4 = new Vertice("4");
		Vertice v5 = new Vertice("5");
		Vertice v6 = new Vertice("6");
		Vertice v7 = new Vertice("7");
		Vertice v8 = new Vertice("8");
		Vertice v9 = new Vertice("9");
		Vertice v10 = new Vertice("10");
		Vertice v11 = new Vertice("11");
		Vertice v12 = new Vertice("12");
		Vertice v13 = new Vertice("13");
		Vertice v14 = new Vertice("14");
		Vertice v15 = new Vertice("15");
		Vertice v16 = new Vertice("16");
		Vertice v17 = new Vertice("17");
		Vertice v18 = new Vertice("18");
		Vertice v19 = new Vertice("19");
		Vertice v20 = new Vertice("20");
		grafo.agregarVertice(v1, v2, v3, v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20);

		grafo.agregarArista(v4, v1);
		grafo.agregarArista(v4, v2);
		grafo.agregarArista(v4, v3);
		grafo.agregarArista(v4, v5);
		grafo.agregarArista(v4, v6);
		grafo.agregarArista(v4, v7);
		grafo.agregarArista(v7, v8);
		grafo.agregarArista(v7, v9);
		grafo.agregarArista(v7, v10);
		grafo.agregarArista(v7, v11);
		grafo.agregarArista(v11, v12);
		grafo.agregarArista(v12, v15);
		grafo.agregarArista(v12, v14);
		grafo.agregarArista(v12, v13);
		grafo.agregarArista(v12, v16);
		grafo.agregarArista(v16,v17);
		grafo.agregarArista(v16,v19);
		grafo.agregarArista(v16,v20);
		grafo.agregarArista(v19,v18);
	}

	private void validacionCasoPruebaConjuntoDominanteMinimoConjuntoResultadoVariosElementosCon15VerticesYConSol6() {
		List<Vertice> conjuntoResultadoEsperado = new ArrayList<>();
		conjuntoResultadoEsperado.add(new Vertice("4"));
		conjuntoResultadoEsperado.add(new Vertice("7"));
		conjuntoResultadoEsperado.add(new Vertice("12"));
		conjuntoResultadoEsperado.add(new Vertice("16"));
		conjuntoResultadoEsperado.add(new Vertice("19"));
		List<Vertice> conjuntoResultadoObtenido = conjuntoDominanteMinimo.obtenerConjuntoDominanteMinimo(grafo);
		
		assertEquals("El conjunto esperado debe ser de longitud 5", conjuntoResultadoEsperado.size(), conjuntoResultadoObtenido.size());
		assertTrue("El conjunto esperado contener el conjunto 4,7,12,16,19", conjuntoResultadoObtenido.containsAll(conjuntoResultadoEsperado));
			
	}
	/*Fin caso de pruebas 1*/
	
}

