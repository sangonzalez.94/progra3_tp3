package progra3_tp3.utilidades.estilos;

public class MiColor {
	public static final String COLOR_AZUL_700 = "#1976d2";
	public static final String COLOR_AZUL_OSCURO = "#004ba0";
	public static final String COLOR_AZUL_CLARO = "#63a4ff";
	
	public static final String COLOR_NARANJA = "#bc5100";
	public static final String COLOR_BLANCO = "#ffffff";
}

