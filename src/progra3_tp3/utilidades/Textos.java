package progra3_tp3.utilidades;

public class Textos {
	public static final String TITULO_APLICACION ="Trabajo Pr�ctico 3 - Conjunto dominante m�nimo";
	
	public static final String CB_TXT_SELECCIONE_OPCION = "Seleccione una opci�n";
	
	public static final String LBL_INGRESE_VERTICE = "<html><body>Ingrese vertice <br> Tambi�n puede ingresarlo mediante fichero CSV respetando <br> un v�rtice por l�nea </body></html>";
	public static final String LBL_NOMBRE_VERTICE = "Nombre de vertice:";
	public static final String LBL_LATITUD = "Latitud:";
	public static final String LBL_LONGITUD = "Longitud:";
	public static final String LBL_CARGAR_ARISTAS = "<html><body>Cree las aristas o carguelas en CSV</body></html>";
	public static final String LBL_SIMILARIDAD ="Similaridad";
	public static final String LBL_CANTIDAD_REGIONES = "Cantidad de regiones";
	public static final String LBL_ALERT_NO_DEBEN_SER_MISMO_VERTICE =	"No deben ser el mismo v�rtice los elegidos";
	public static final String LBL_ALERT_INGRESAR_SIMILARIDAD = "Debes ingresar la similaridad";
	public static final String BTN_AGREGAR_VERTICE =  "Agregar Vertice";
	public static final String BTN_SUBIR_CSV = "Subir CSV";
	public static final String BTN_AGREGAR_ARISTA = "Agregar arista";
	public static final String BTN_VER_GRAFO_ACTUAL = "Ver grafo actual";
	public static final String BTN_EJECUTAR_ALGORITMO_GOLOSO = "Ejecutar algoritmo goloso";
	public static final String LBL_CONJUNTO_RESULTADO = "Conjunto resultado: ";
}
