package progra3_tp3.view;

import static progra3_tp3.utilidades.Textos.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang3.StringUtils;
import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultListenableGraph;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.swing.mxGraphComponent;

import progra3_tp3.controller.LauncherController;
import progra3_tp3.model.grafo.Arista;
import progra3_tp3.model.grafo.Vertice;
import progra3_tp3.utilidades.estilos.MiColor;

public class Launcher implements ActionListener {
	private static final Dimension DEFAULT_SIZE = new Dimension(530, 320);
	private JGraphXAdapter<String, DefaultEdge> jgxAdapter;
	private ListenableGraph<String, DefaultEdge> g;

	// Swing
	private JFrame frame;
	private JPanel panelIngresoDeDatos, panelGrafico1;
	private JButton btnVerGrafoActual, btnLimpiarPantalla, btnEjecutarAlgoritmoGoloso, btnAgregarVertice,
			btnAgregarArista, btnSubirCsv1, btnSubirCsvAristas;
	private JTextField tfNombreVertice;
	private JLabel lblCrearAristas, lblConjuntoResultado;
	private JTable tblFronterasSimilaridad;
	private JLabel JLNombreVertice;
	private JComboBox<Vertice> cbVertice1, cbVertice2;

	private LauncherController launcherController;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Launcher window = new Launcher();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Launcher() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		/**/
		frame = new JFrame();
		frame.setBounds(100, 100, 1024, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle(TITULO_APLICACION);
		try {
			JFrame.setDefaultLookAndFeelDecorated(true);
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			//UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroDefaultTheme");
			//UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel"); //funciona
			//UIManager.setLookAndFeel("org.mpizexternal.MaterialLookAndFeel"); //funciona
		} catch (Exception e) {
			e.printStackTrace();
		}
		launcherController = new LauncherController();
		generarPanelesDeLaPantalla();

		generarElementosDePantallaCargarInformacion();

	}

	private void generarPanelesDeLaPantalla() {
		panelIngresoDeDatos = new JPanel();
		panelIngresoDeDatos.setBounds(0, 0, 1024, 768);
		panelIngresoDeDatos.setLayout(null);
		panelIngresoDeDatos.setVisible(true);
		panelIngresoDeDatos.setBackground(Color.decode("#efefef"));
		frame.getContentPane().add(panelIngresoDeDatos);
	}

	private void generarElementosDePantallaCargarInformacion() {
		JLabel lblIngreseVertice = new JLabel(LBL_INGRESE_VERTICE);
		lblIngreseVertice.setBounds(20, 11, 331, 83);
		panelIngresoDeDatos.add(lblIngreseVertice);
		/*   #### 	panel grupo 1 	###*/
		
		JPanel panelGrupo1  = new JPanel();
		panelGrupo1.setBounds(10, 11, 374, 198);
		panelGrupo1.setLayout(null);
		panelGrupo1.setVisible(true);
		panelGrupo1.setBackground(Color.decode("#ffffff"));
		/* Nombre de vertice */

		tfNombreVertice = new JTextField();
		tfNombreVertice.setBounds(183, 95, 182, 23);
		panelGrupo1.add(tfNombreVertice);
		tfNombreVertice.setColumns(10);
		/* Fin de nombre de vertice */

		/* Boton de agregar vertice */
		btnAgregarVertice = new JButton(BTN_AGREGAR_VERTICE);
		btnAgregarVertice.setBounds(10, 120, 169, 23);
		btnAgregarVertice.addActionListener(this);
		btnAgregarVertice.setBackground(Color.decode(MiColor.COLOR_AZUL_700));
		panelGrupo1.add(btnAgregarVertice);
		/* Fin boton agregar vertice */

		/* Boton de subir csv 1 */
		btnSubirCsv1 = new JButton(BTN_SUBIR_CSV);
		btnSubirCsv1.setBounds(183, 120, 182, 23);
		btnSubirCsv1.addActionListener(this);
		btnSubirCsv1.setBackground(Color.decode(MiColor.COLOR_AZUL_700));
		panelGrupo1.add(btnSubirCsv1);
		/* Fin boton subir csv 1 */
		JLNombreVertice = new JLabel(LBL_NOMBRE_VERTICE);
		JLNombreVertice.setBounds(10, 87, 140, 14);
		panelGrupo1.add(JLNombreVertice);
		panelIngresoDeDatos.add(panelGrupo1);
		
		/*######## FIN GRUPO 1 ####### */
		
		
		/*######## GRUPO 2 ####### */
		JPanel panelGrupo2  = new JPanel();
		panelGrupo2.setBounds(10, 261, 374, 121);
		panelGrupo2.setLayout(null);
		panelGrupo2.setVisible(true);
		panelGrupo2.setBackground(Color.decode("#ffffff"));
		
		lblCrearAristas = new JLabel(LBL_CARGAR_ARISTAS);
		lblCrearAristas.setBounds(10, 10, 331, 23);
		panelGrupo2.add(lblCrearAristas);

		/* COMBOS */

		cbVertice1 = new JComboBox<>();
		cbVertice1.setBounds(10, 44, 169, 23);
		panelGrupo2.add(cbVertice1);

		cbVertice2 = new JComboBox<>();
		cbVertice2.setBounds(183, 44, 169, 23);
		panelGrupo2.add(cbVertice2);
		/* FIN TRATAMIENTO DE COMBOS */

		btnAgregarArista = new JButton(BTN_AGREGAR_ARISTA);
		btnAgregarArista.addActionListener(this);
		btnAgregarArista.setBackground(Color.decode(MiColor.COLOR_AZUL_700));
		btnAgregarArista.setBounds(10, 82, 169, 23);
		panelGrupo2.add(btnAgregarArista);
		
		btnSubirCsvAristas = new JButton(BTN_SUBIR_CSV);
		btnSubirCsvAristas.addActionListener(this);
		btnSubirCsvAristas.setBackground(Color.decode(MiColor.COLOR_AZUL_700));
		btnSubirCsvAristas.setBounds(183, 82, 169, 23);
		panelGrupo2.add(btnSubirCsvAristas);
		
		panelIngresoDeDatos.add(panelGrupo2);
		
		
		
		/*######## FIN GRUPO 1 ####### */
		tblFronterasSimilaridad = new JTable();
		tblFronterasSimilaridad.setEnabled(false);
		tblFronterasSimilaridad.setBounds(10, 393, 374, 278);
		panelIngresoDeDatos.add(tblFronterasSimilaridad);

		btnVerGrafoActual = new JButton(BTN_VER_GRAFO_ACTUAL);
		btnVerGrafoActual.setBackground(Color.decode(MiColor.COLOR_AZUL_OSCURO));
		btnVerGrafoActual.addActionListener(this);
		btnVerGrafoActual.setBounds(390, 11, 287, 23);
		btnVerGrafoActual.addActionListener(this);
		panelIngresoDeDatos.add(btnVerGrafoActual);

		btnEjecutarAlgoritmoGoloso = new JButton(BTN_EJECUTAR_ALGORITMO_GOLOSO);
		btnEjecutarAlgoritmoGoloso.setBackground(Color.decode(MiColor.COLOR_AZUL_OSCURO));
		btnEjecutarAlgoritmoGoloso.addActionListener(this);
		btnEjecutarAlgoritmoGoloso.setBounds(703, 11, 287, 23);
		panelIngresoDeDatos.add(btnEjecutarAlgoritmoGoloso);

		btnLimpiarPantalla = new JButton("Limpiar pantalla");
		btnLimpiarPantalla.addActionListener(this);
		btnLimpiarPantalla.setBackground(Color.decode(MiColor.COLOR_NARANJA));
		btnLimpiarPantalla.setBounds(10, 681, 374, 23);
		panelIngresoDeDatos.add(btnLimpiarPantalla);

		

		launcherController.crearNuevosModel();

		// create a JGraphT graph
		panelGrafico1 = new JPanel();
		panelGrafico1.setBounds(0, 0, 0, 0);

		// this.cargarModel();
		g = new DefaultListenableGraph<>(new DefaultDirectedGraph<>(DefaultEdge.class));
		jgxAdapter = new JGraphXAdapter<String, DefaultEdge>(g);

		mxGraphComponent component = new mxGraphComponent(jgxAdapter);
		component.setConnectable(false);
		component.getGraph().setAllowDanglingEdges(false);
		component.setSize(DEFAULT_SIZE);
		component.setBounds(390, 55, 600, 290);
		panelIngresoDeDatos.add(component);

		lblConjuntoResultado = new JLabel(LBL_CONJUNTO_RESULTADO);
		lblConjuntoResultado.setBounds(390, 366, 601, 108);
		panelIngresoDeDatos.add(lblConjuntoResultado);

		this.cargarModel();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnAgregarVertice) {
			accionAgregarVertice();
		} else if (e.getSource() == btnAgregarArista) {
			accionAgregarArista();
		} else if (e.getSource() == btnEjecutarAlgoritmoGoloso) {
			accionEjecutarAlgoritmoGoloso();
		} else if (e.getSource() == btnVerGrafoActual) {
			accionGrafoActual();
		} else if (e.getSource() == btnLimpiarPantalla) {
			accionLimpiarPantalla();
		} else if (e.getSource() == btnSubirCsv1) {
			try {
				accionBtnSubirCsvVertices();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (e.getSource() == btnSubirCsvAristas) {
			try {
				accionBtnSubirCsvAristas();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	private void accionBtnSubirCsvAristas() throws IOException {
		File archivo = abrirArchivoConPopUp();
		BufferedReader br = null;
	      try {
	         br =new BufferedReader(new FileReader(archivo));
	         String line = br.readLine();
	         while (null!=line) {
	            String [] fields = line.split(",");
	            if(!line.isEmpty()) {
	            	launcherController.agregarArista(new Vertice(fields[0]), new Vertice(fields[1]));
	            }
	            line = br.readLine();
	         }
	         
	      } catch (Exception e) {

	      } finally {
	         if (null!=br) {
	            br.close();
	         }
	      }
	}

	private void accionBtnSubirCsvVertices() throws IOException {
		File archivo = abrirArchivoConPopUp();
		BufferedReader br = null;
	      try {
	         
	         br =new BufferedReader(new FileReader(archivo));
	         String line = br.readLine();
	         while (null!=line) {
	            if(!line.trim().isEmpty()) {
	            	launcherController.addVertice(line);
	            }
	            line = br.readLine();
	         }
	         
	      } catch (Exception e) {

	      } finally {
	         if (null!=br) {
	            br.close();
	         }
	      }
	}

	private File abrirArchivoConPopUp() {
		File fileReturn = null;

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		FileNameExtensionFilter imgFilter = new FileNameExtensionFilter("Solo archivos CSV", "csv");
		fileChooser.setFileFilter(imgFilter);

		int result = fileChooser.showOpenDialog(frame);

		if (result != JFileChooser.CANCEL_OPTION) {

			fileReturn = fileChooser.getSelectedFile();
			if ((fileReturn == null) || (fileReturn.getName().equals(""))) {
				return null;
			} else {
				return fileReturn;
			}
		}
		return null;
	}

	private void accionLimpiarPantalla() {
		int respuesta = JOptionPane.showConfirmDialog(panelIngresoDeDatos, "�Desea limpiar la pantalla?",
				"LIMPIAR PANTALLA", JOptionPane.YES_NO_OPTION);
		if (respuesta == JOptionPane.YES_OPTION) {
			//launcherController = new LauncherController();
			launcherController.crearNuevosModel();
			cargarModel();
			limpiarTextFields();
		}
	}

	private void cargarModel() {
		cbVertice1.setModel(launcherController.getComboModel1());
		cbVertice2.setModel(launcherController.getComboModel2());
		tblFronterasSimilaridad.setModel(launcherController.getTableModel());
	}

	/**
	 * Muestra el grafo actual.
	 */
	private void accionGrafoActual() {
		List<Vertice> verticesGM = launcherController.getVerticesGrafoOriginal();
		for (Vertice verticeX : verticesGM) {
			g.addVertex(verticeX.getNombre());
		}
		for (Arista aristaX : launcherController.getGrafoMapaOriginal().getAristas()) {
			g.addEdge(aristaX.getVerticeA().getNombre(), aristaX.getVerticeB().getNombre());
		}
		// positioning via jgraphx layouts
		mxCircleLayout layout = new mxCircleLayout(jgxAdapter);
		// center the circle
		int radius = 100;
		layout.setX0((DEFAULT_SIZE.width / 2.0) - radius);
		layout.setY0((DEFAULT_SIZE.height / 2.0) - radius);
		layout.setRadius(radius);
		layout.setMoveCircle(true);

		layout.execute(jgxAdapter.getDefaultParent());
	}

	private void accionEjecutarAlgoritmoGoloso() {
		List<Vertice> verticesConjuntoDominanteMinimo = launcherController.getConjuntoDominanteMinimo();
		List<String> nombresVertices = new ArrayList<>();
		for (Vertice v : verticesConjuntoDominanteMinimo) {
			nombresVertices.add(v.getNombre());
		}

		lblConjuntoResultado.setText(LBL_CONJUNTO_RESULTADO + " " + StringUtils.join(nombresVertices, ","));
	}

	private void accionAgregarArista() {
		if (cbVertice1.getSelectedItem().toString().equals(cbVertice2.getSelectedItem().toString())) {
			JOptionPane.showMessageDialog(panelIngresoDeDatos, LBL_ALERT_NO_DEBEN_SER_MISMO_VERTICE);
		} else {
			launcherController.agregarArista(
					launcherController.getMapStringVertice().get(cbVertice1.getSelectedItem().toString()),
					launcherController.getMapStringVertice().get(cbVertice2.getSelectedItem().toString()));
			tblFronterasSimilaridad.revalidate();
		}
	}

	private void accionAgregarVertice() {
		String mensajesDeError = "";
		if (!verificarTfField(tfNombreVertice)) {
			mensajesDeError = mensajesDeError + ("Deb�s ingresar un nombre\n");
		}

		launcherController.addVertice(tfNombreVertice.getText());
		limpiarTextFields();
		btnVerGrafoActual.transferFocus();

		if (!mensajesDeError.isEmpty()) {

			JOptionPane.showMessageDialog(panelIngresoDeDatos, mensajesDeError);
		}
	}

	private boolean verificarTfField(JTextField tf) {
		if (tf == null || tf.getText().isEmpty()) {
			return false;
		}
		return true;

	}

	private void limpiarTextFields() {
		tfNombreVertice.setText("");
		lblConjuntoResultado.setText(LBL_CONJUNTO_RESULTADO);
	}
}
