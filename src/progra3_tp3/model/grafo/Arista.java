package progra3_tp3.model.grafo;

public class Arista {
	//Entre las dos vertices que hay una arista o una similaridad.
		private Vertice verticeA; 
		private Vertice verticeB; 
		
		public Vertice getVerticeA() {
			return verticeA;
		}

		public void setVerticeA(Vertice verticaA) {
			this.verticeA = verticaA;
		}

		public Vertice getVerticeB() {
			return verticeB;
		}

		public void setVerticeB(Vertice verticeB) {
			this.verticeB = verticeB;
		}


		public Arista() {
			super();
		}
		
		public Arista(Vertice verticaA, Vertice verticeB) {
			super();
			this.verticeA = verticaA;
			this.verticeB = verticeB;
		}
		
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((verticeA == null) ? 0 : verticeA.hashCode());
			result = prime * result + ((verticeB == null) ? 0 : verticeB.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Arista other = (Arista) obj;
			if (verticeA == null) {
				if (other.verticeA != null)
					return false;
			} else if (!verticeA.equals(other.verticeA))
				return false;
			if (verticeB == null) {
				if (other.verticeB != null)
					return false;
			} else if (!verticeB.equals(other.verticeB))
				return false;
			return true;
		}
		
		public String[] getDataEnArregloDeString() {
			return new String[] { verticeA.getNombre() , verticeB.getNombre()};
		}
		
}
