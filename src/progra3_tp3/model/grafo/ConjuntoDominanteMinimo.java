package progra3_tp3.model.grafo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
public class ConjuntoDominanteMinimo {
	
	/*
	 * Este mapa tiene por cada vertice del grafo, la cantidad de vecinos de la arista.
	 */
	private Map<Vertice, Integer> mapVerticeCantidadAristas = new HashMap<>();
	
	/*
	 * Este mapa tiene como clave un numero que significa la cantidad de vecinos que tienen los
	 * vertices del valor del mapa.
	 */
	private Map<Integer, List<Vertice>> mapCantAristasVertices = new HashMap<>();
	
	/*
	 * este mapa tiene todos los vertices del grafo y un booleano. Si este esta en
	 * verdadero, es que este ya fue alcanzado por algun elemento del conjunto dominante minimo.
	 */
	private Map<Vertice, Boolean> mapVerticesAlcanzados = new HashMap<>();
	
	/*
	 * Esta lista tiene los vertices que forman parte del conjunto dominante minimo.
	 */
	private List<Vertice> conjuntoDominanteMinimo = new ArrayList<>();
	
	
	/**
	 * Devuelve una lista de vertices que corresponden al conjunto dominante m�nimo a
	 * partir de un grafo recibido por parametro.
	 * @param g un grafo conexo
	 * @return lista de vertices.
	 */
	public List<Vertice> obtenerConjuntoDominanteMinimo(Grafo g) {
		
		//1 - Verifico que el grafo sea conexo
		if(!BFS.esConexo(g)) {
			throw new IllegalArgumentException("El grafo debe ser conexo");
		}
		
		/*
		 * 2 - Lleno las variables con los valres provenientes del grafo. 
		 */
		for (Vertice vertice:g.getVertices()) {
			mapVerticeCantidadAristas.put(vertice, g.getVecinos().get(vertice).size());
			mapVerticesAlcanzados.put(vertice, false);
		}
		for(Entry<Vertice, Integer> m : mapVerticeCantidadAristas.entrySet()){    
		    if(mapCantAristasVertices.containsKey(m.getValue())) {
		    	mapCantAristasVertices.get(m.getValue()).add(m.getKey());
		    }else {
		    	List <Vertice> vertices = new ArrayList<>();
		    	vertices.add(m.getKey());
		    	mapCantAristasVertices.put(m.getValue(), vertices);
		    }
		}
		
		/*
		 * 3 - Ordeno las variables para poder trabajar.
		 * 		a) hago una lista con los grados del vertice 
		 * 		b) ordeno la lista de modo descendente.
		 */
		List<Integer> gradosDelGrafo = new ArrayList<>(mapCantAristasVertices.keySet());
		Collections.sort(gradosDelGrafo);
		Collections.reverse(gradosDelGrafo);
		
		/*
		 * el contador de grado mas alto tiene el grado m�s alto. Con dicho valor, 
		 * lo remuevo del mapa correspondiente y me traigo una lista de vertices.
		 * */
		int contadorGradoMasAlto = gradosDelGrafo.remove(0);
		List<Vertice> vertices = mapCantAristasVertices.remove(contadorGradoMasAlto);
		
		/*
		 * A continuaci�n se hace el conjunto dominante minimo. 
		 */
		boolean primerEjecucion = true;
		do {
			
			/* En cada iteraci�n se saca un vertice de la lista. Si todavia no fueron alcanzados todos los 
			 * vertices por el conjunto dominante, actualizo con contador con el proximo valor del mapa 
			 * con el siguiente grado m�s alto
			*/
			if(vertices.isEmpty()) {
				contadorGradoMasAlto=gradosDelGrafo.remove(0);
				vertices = mapCantAristasVertices.remove(contadorGradoMasAlto);
			}
			Vertice vertice = null;
			/*
			 * Si la ejecucion es la primera, remuevo el primer vertice y cambio el valor de primerEjecucion*/
			if(primerEjecucion) {
				vertice = vertices.remove(0);
				primerEjecucion=false;
			}else {
				/*
				 * en todos los dem�s casos, busco el siguiente vertice con m�s vertices sin alcanzar. */
				vertice = siguienteVerticeConMasVerticesSinAlcanzar(g, vertices, mapVerticesAlcanzados);
				/*una vez encontrado el vertice, lo remuevo de la lista de vertices*/
				vertices.remove(vertice);
			}
			/*Obtengo los vecinos del vertice y a�ado en la lista tambi�n al vertice del cual obtuve 
			 * los vecinos, para que todos los vertices esten marcados como visitados o alcanzados en 
			 * el hashmap correspondiente.
			 * */
			Set<Vertice> vecinos = g.vecinosDeVertice(vertice);
			vecinos.add(vertice);
			
			/*Recorro los vertices del set anterior y los marco como alcanzados*/
			boolean nuevosVerticesAlcanzados = false;
			for(Vertice verticeVecino:vecinos) {
				mapVerticesAlcanzados.put(verticeVecino, true);
				nuevosVerticesAlcanzados = true;
			}
			if(nuevosVerticesAlcanzados) {
				conjuntoDominanteMinimo.add(vertice);
			}
		} while( hayVerticesAlcanzadosConFalse(mapVerticesAlcanzados));
			
		
		
		return conjuntoDominanteMinimo;
	}

	/**
	 * Devuelve el siguiente vertice con m�s vertices sin alcanzar a partir de un grafo dado
	 * @param grafo grafo donde se encuentran los vertices.
	 * @param vertices vertices a evaluar si son o no alcanzados.
	 * @param mapVerticesAlcanzados mapa con los vertices alcanzados
	 * @return 
	 */
	private Vertice siguienteVerticeConMasVerticesSinAlcanzar(Grafo grafo, List<Vertice> vertices,
			Map<Vertice, Boolean> mapVerticesAlcanzados) {
		Map<Vertice, Integer> mapVertCantSinAlcanzar = new HashMap<>();
		for(Vertice v : vertices) {
			 Set<Vertice> vecinos = grafo.vecinosDeVertice(v);
			 Integer verticesVecinosSinAlcanzar = 0;
			 for(Vertice vAlcanzado :vecinos) {
				 if(!mapVerticesAlcanzados.get(vAlcanzado)) {
					 verticesVecinosSinAlcanzar++;
				 }
			 }
			 mapVertCantSinAlcanzar.put(v, verticesVecinosSinAlcanzar);
		}
		
		List<Integer> verticesVecinosSinAlcanzarMayor = new ArrayList<>(mapVertCantSinAlcanzar.values());

		Collections.sort(verticesVecinosSinAlcanzarMayor);
		Collections.reverse(verticesVecinosSinAlcanzarMayor);
		
		Integer mayorCantidadVecinosSinAlcanzar = verticesVecinosSinAlcanzarMayor.get(0);
		for(Entry<Vertice, Integer> m : mapVertCantSinAlcanzar.entrySet()){    
			if(m.getValue()==mayorCantidadVecinosSinAlcanzar) {
				return m.getKey();
			}
		}
		return null;
	}



	private boolean hayVerticesAlcanzadosConFalse(Map<Vertice, Boolean> mapVerticesAlcanzados) {
		boolean verticesConFalse = false;		
		for(Entry<Vertice, Boolean> m : mapVerticesAlcanzados.entrySet()){    
		    verticesConFalse = verticesConFalse || !m.getValue();
		}
		return verticesConFalse;
	}
	

}
