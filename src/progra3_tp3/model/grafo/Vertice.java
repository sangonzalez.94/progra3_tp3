package progra3_tp3.model.grafo;

/**
 * El vertice representa a las provincias, ciudades, etc.
 * @author a765168
 *
 */
public class Vertice {
	
	private String nombre;

	public Vertice(String nombre) {
		super();
		this.nombre = nombre;
	
	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Vertice v = (Vertice) obj;
		if(this == obj) {
			return true;
		}
		String nombreA=this.getNombre().toUpperCase();
		String nombreB= v.getNombre().toUpperCase();
		if(nombreA.equals(nombreB)) {
			return true;
		}
		return false;
	}
	
	
}
