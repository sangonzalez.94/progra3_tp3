package progra3_tp3.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import progra3_tp3.model.ComboModel;
import progra3_tp3.model.TableModel;
import progra3_tp3.model.grafo.Arista;
import progra3_tp3.model.grafo.BFS;
import progra3_tp3.model.grafo.ConjuntoDominanteMinimo;
import progra3_tp3.model.grafo.Grafo;
import progra3_tp3.model.grafo.Vertice;

public class LauncherController {
	
	private TableModel tableModel;
	private ComboModel comboModel1;
	private ComboModel comboModel2;
	
	private Map<String, Vertice> mapStringVertice;
	private ArrayList<Arista> aristas;
	private ArrayList<Vertice> vertices;
	
	private Grafo grafoOriginal;
	private Grafo grafo;
	
	public LauncherController() {
		vertices = new ArrayList<>();
		aristas = new ArrayList<>();
		mapStringVertice = new HashMap<>();

	}
	public void crearNuevosModel() {
		comboModel1 = new ComboModel();
		comboModel2 = new ComboModel();
		tableModel = new TableModel();
		tableModel.addColumn("ig");
		tableModel.addColumn("Frontera");
		vertices = new ArrayList<>();
		aristas = new ArrayList<>();
		mapStringVertice = new HashMap<>();
		grafoOriginal = new Grafo();
		grafo = new Grafo();
	}
	
	public TableModel getTableModel() {
		return tableModel;
	}
	
	public ComboModel getComboModel1() {
		return this.comboModel1;
	}
	
	public ComboModel getComboModel2() {
		return this.comboModel2;
	}
	
	public void anadirElementoAlComboModel1(Vertice vertice) {
		this.comboModel1.addElement(vertice);
	}
	
	public void anadirElementoAlComboModel2(Vertice vertice) {
		this.comboModel2.addElement(vertice);
	}
	
	public List<Vertice> getConjuntoDominanteMinimo() {
		grafo = new Grafo();
		ConjuntoDominanteMinimo conjuntoDominanteMinimo = new ConjuntoDominanteMinimo();
		for(Vertice vertice:vertices) {
			grafo.agregarVertice(vertice);
		}

		for(Arista arista:aristas) {
			grafo.agregarArista(arista.getVerticeA(), arista.getVerticeB());
		}

		if(!BFS.esConexo(grafo)) {
			throw new IllegalArgumentException("El grafo no es conexo");
		}

		List<Vertice> verticesGM = conjuntoDominanteMinimo.obtenerConjuntoDominanteMinimo(grafo);
		return verticesGM;
	}
	public Map<String, Vertice> getMapStringVertice() {
		return mapStringVertice;
	}
	public void setMapStringVertice(Map<String, Vertice> mapStringVertice) {
		this.mapStringVertice = mapStringVertice;
	}
	public ArrayList<Arista> getFronteras() {
		return aristas;
	}
	public void setFronteras(ArrayList<Arista> fronteras) {
		this.aristas = fronteras;
	}
	public ArrayList<Vertice> getProvincias() {
		return vertices;
	}
	public void setProvincias(ArrayList<Vertice> provincias) {
		this.vertices = provincias;
	}
	
	
	public void agregarArista(Vertice vertice, Vertice vertice2) {
		Arista a = new Arista(vertice, vertice2);
		this.aristas.add(a);
		this.tableModel.addRow(a.getDataEnArregloDeString());
	}
	public void addVertice(String text) {
		Vertice v = new Vertice(text);
		if(!this.vertices.contains(v)) {
			this.vertices.add(v);
			this.getMapStringVertice().put(text, v);
			this.anadirElementoAlComboModel1(v);
			this.anadirElementoAlComboModel2(v);
		}
	}
	public Grafo getGrafoMapaOriginal() {
		return grafoOriginal;
	}
	public void setGrafoMapaOriginal(Grafo grafo) {
		this.grafoOriginal = grafo;
	}
	
	public List<Vertice> getVerticesGrafoOriginal() {
		grafoOriginal = new Grafo();
		for(Vertice vertice:vertices) {
			grafoOriginal.agregarVertice(vertice);
		}
		for(Arista arista:aristas) {
			grafoOriginal.agregarArista(arista.getVerticeA(), arista.getVerticeB());
		}
		if(!BFS.esConexo(grafoOriginal)) {
			throw new IllegalArgumentException("El grafo no es conexo");
		}

		List<Vertice> verticesGM = grafoOriginal.getVertices();
		return verticesGM;
	}
	
	
}
